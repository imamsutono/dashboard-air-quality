<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Air Quality Dashboard</title>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <h1>Air Quality Dashboard</h1>

    <label for="timeSeries">Time Series</label>
    <select name="time_series" id="timeSeries">
        <option value="hourly">hourly</option>
        <option value="daily">daily</option>
        <option value="monthly">monthly</option>
    </select>

    <canvas id="myChart" style="width:100%;"></canvas>

    <script>
        const xValues = {!! json_encode($time_series) !!}
        const yValues = {!! json_encode($values) !!}
        const ctx = document.getElementById('myChart').getContext('2d')

        new Chart(ctx, {
            type: "line",
            data: {
                labels: xValues,
                datasets: [{
                    label: 'Air Quality',
                    fill: true,
                    tension: 0.1,
                    backgroundColor: "rgba(0,0,255,1.0)",
                    borderColor: "rgba(0,0,255,0.1)",
                    data: yValues
                }]
            },
        })
    </script>
</body>
</html>