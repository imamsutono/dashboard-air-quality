<?php

$url = 'https://api.openaq.org';
$version = 'v2';

return [
    'base_url' => $url,
    'version'  => $version,
    'full_url' => "$url/$version"
];
