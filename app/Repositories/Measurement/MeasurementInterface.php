<?php

namespace App\Repositories\Measurement;

interface MeasurementInterface
{
    const DAILY = 'daily';
    const HOURLY = 'hourly';
    const MONTHLY = 'monthly';

    public function getByLocation(int $locationId, int $limit = 1000): array;
    public function getTimeSeries(string $mode): array;
}
