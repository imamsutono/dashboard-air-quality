<?php

namespace App\Repositories\Measurement;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class MeasurementRepository implements MeasurementInterface
{
    public function getByLocation(int $locationId, int $limit = 1000): array
    {
        $url = config('openaqapi.full_url') . '/measurements';
        $response = Http::acceptJson()->get($url, [
            'location_id' => $locationId,
            'parameter'   => 'pm25',
            'unit'        => 'µg/m³',
            'limit'       => $limit
        ]);

        return $response->json('results');
    }

    public function getTimeSeries(string $mode): array
    {
        return match ($mode) {
            self::HOURLY  => range(0, 23),
            self::DAILY   => range(1, Carbon::now()->daysInMonth),
            self::MONTHLY => range(1, 12)
        };
    }
}
