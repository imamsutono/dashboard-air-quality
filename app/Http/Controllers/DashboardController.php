<?php

namespace App\Http\Controllers;

use App\Repositories\Measurement\MeasurementRepository;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function __construct(protected MeasurementRepository $measurement)
    {
    }

    public function index(): View
    {
        $data = $this->measurement->getByLocation(1628560, 24);
        $value = collect($data)->pluck('value');
        $timeSeries = $this->measurement->getTimeSeries('hourly');

        return view('dashboard', [
            'values' => $value,
            'time_series' => $timeSeries
        ]);
    }
}
